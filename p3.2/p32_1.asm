.data
numeros: .word 8, -7
resultado: .word 0

.text
.globl main
main:

la $a0, numeros	# Cargar la dirección de los números en ($a0)
lw $t1, 0($a0)	# Cargar el primer número
lw $t2, 4($a0)	# Cargar el segundo número (4 bytes a continuación)

add $t3, $t1, $t2	# Suma los números y lo almacena en ($t3)
la $a1, resultado	# Cargar la dirección del resultado en ($a1)
sw $t3, 0($a1)	# Guardar el resultado

addi $v0, $0, 10	# END
syscall