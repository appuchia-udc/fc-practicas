.data
numeros: .word 8, 7, 9, 6, 2, 3	# Vector de números a sumar como palabras de 32 bits
size: .word 6	# Tamaño del vector
resultado: .word 0	# Resultado

.text
.globl main
main:
la $a0, numeros	# Cargar la dirección de los números en ($a0)
la $a1, size	# Cargar la dirección del tamaño del vector
lw $t2, 0($a1)	# Cargar el valor del tamaño del vector
addi $a1, $0, 0	# Vaciar $a1 (No original)

addi $t0, $0, 0	# Inicializar el resultado a 0

loop:	# Calcular la suma iterativamente
lw $t1, 0($a0)	# Cargar el primer valor
add $t0, $t0, $t1	# Realizar la suma
addi $a0, $a0, 4	# Cambiar la dirección base al sigueinte número
addi $t2, $t2, -1	# Decrementar el contador
bne $t2, $0, loop	# Iterar si quedan números por cargar

la $a1, resultado	# Cargar la dirección del resultado en ($a1)
sw $t0, 0($a1)	# Guardar el resultado

addi $v0, $0, 10	# END
syscall