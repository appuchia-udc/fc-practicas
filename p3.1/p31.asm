# .data
# cadena: .asciiz "Hola mundo!"

# .text
# .globl main
# main:
# la $a0, cadena
# addi $v0, $0, 4
# syscall

# addi $v0, $0, 10    # Fin de
# syscall             # programa

# A partir de la 0x10010000. En el segmento de datos

# la permite trabajar con tags
# En lui y ori
    # En el segmento de texto

    # En 0x00400000 y 0x00400004

    # La primera es de tipo I: 0x3c040000
    # 001111 - 15: operaci'on
    # 00000  -  0: origen
    # 00100  -  4: destino
    # 0000000000000000: dato inmediato
    # La segunda es de tipo I: 0x34840000
    # 001101 - 13: operaci'on
    # 00100  -  4: origen
    # 00100  -  4: destino
    # 0000000000000000: dato inmediato

    # Ni idea, el OR es con 0 asi que siempre sera $a0

# En la 0x00400008 - 0000 0000 0100 0000 0000 0000 0000 1000
# 000000: Op.
# 00010 : Origen
# 00000 : Destino
# 0000000000001000: Inmediato (4)

# $a0 queda con 10010000 (Memoria de "hola mundo") y $v0 con 0000000a (10)

# 2.
# .data
# cadena: .asciiz "hola mundo"

# .text
# .globl main
# main:

# lazo:
# la $a0, cadena
# addi $v0, $0, 4
# syscall
# j lazo

# addi $v0, $0, 10
# syscall

# En 0x00400010

# Apunta a 0000 : 00 0001 0000 0000 0000 0000 0000 : 00 -> 0000 0000 0100 0000 0000 0000 0000 0000

# La representación en hex es 0x08100000, en binario es 0010 00 00 0001 0000 0000 0000 0000 0000
# La dirección de salto es PC[0] * 4 : 26 bits : 00
# Utiliza direccionamiento pseudo-directo

# 3.
.data
cadena: .asciiz "Hola mundo!"

.text
.globl main
main:
addi $7, $0, 3 # Número de iteraciones

# lazo:
# beq $4, $4, end
# la $3, cadena
# addi $2, $0, 4
# syscall
# addi $4, $4, -1
# j lazo

lazo:
la $4, cadena
addi $v0, $0, 4
syscall
addi $7, $7, -1 # Restar 1 al contador
bne $7, $0, lazo # Si el contador no es 0, repetir

# end:
addi $v0, $0, 10
syscall

# En 0x00400018

# 0x14e0(fffa)

# Direccionamiento con desplazamiento relativo al PC

# Es 000101 00111 00000 (1111 1111 1111 1010)
# En complemento a 2. 1111 1111 1111 1010 = -6 instrucciones
# 0x0040001c - 0x00000006 * 4 = 0x0040001c - 0x00400018 = 0x00400004

# Optimizado
.data
cadena: .asciiz "Hola mundo!"

.text
.globl main
main:
addi $7, $0, 15 # Número de iteraciones
la $4, cadena
addi $v0, $0, 4

lazo:
syscall
add $15, $0, $7
addi $7, $7, -1 # Restar 1 al contador
bne $7, $0, lazo # Si el contador no es 0, repetir

addi $v0, $0, 10
syscall
