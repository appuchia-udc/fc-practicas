.data		# Entrada de datos
nombre: .asciiz "Nombre"	# String terminado en 0x00
numero: .word 5, -5		# C-2 en 32b
flotante: .float 5		# IEEE 754

.text		# [
.globl main		#  Boilerplate
main:		#                      ]
la $a0, nombre		
addi $v0, $0, 4		# Código de output a pantalla
syscall		# Interrupción al SO

addi $v0, $0, 10		# Código de fin de programa
syscall		# Interrupción al SO