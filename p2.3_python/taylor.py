#! /bin/env python3

print("\n****************************************************")
print("Resolver la suma de una serie con la mayor precision")
print("****************************************************\n")

serie = [j for j in range(10)]

for j in range(10):
    serie.append(1.0/(2.5**j))

serie.sort()
print("**** La serie ordenada de menor a mayor es: ****")
print(serie)


suma =0.0
for i in range(20):
    suma = suma + serie[i]

print("\nSumando de menor a mayor: %.12lf (con 12 decimales) o %.20lf (con 20 decimales) " % (suma, suma))

suma2 =0.0
for i in range(20):
    suma2 = suma2 + serie[-(1+i)]


print("\nSumando de mayor a menor: %.12lf (con 12 decimales) o %.20lf (con 20 decimales)\n " % (suma2, suma2))

print('Delta: ' + str(suma - suma2))
print(f'Usando sum() en la lista ordenada:  {sum(serie):.20f}')
print(f'Usando sum() en la lista invertida: {sum(reversed(serie)):.20f}')

