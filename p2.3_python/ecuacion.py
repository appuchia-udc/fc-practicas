#! /bin/env python2

print "\n***************************************************"
print "Resolver (x2-y2)=(x+y)(x-y) con la mayor precision"
print "***************************************************\n"

x = 1.023841858
print "x = %.10lf" % x
y = 1.011920929
print "y = %.10lf\n" % y

x2 = x*x
y2 = y*y

e1=x2-y2
print "(x2 - y2) = %.10f (con 10 decimales) o %.20lf (con 20 decimales)\n" % (e1, e1)

s = x + y
d = x - y

e2 = s*d
print "(x + y) (x - y) = %.10f (con 10 decimales) o %.20lf (con 20 decimales)\n" % (e2, e2)

print("Delta: " + str(e1 - e2))

