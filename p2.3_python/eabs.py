#! /bin/env python2

print "\n************************************************"
print "Error cometido al operar con el flotante 0.1"
print "************************************************\n"

suma =0.0
for i in range(10000000):
    suma = suma + 0.1
error=10000000/10 - suma

print "\n***********************************************************************"
print "Sumandolo 10 millones de veces, el error absoluto cometido es: %.6f" % error
print "***********************************************************************\n"


suma =0.0
for i in range(100000000):
    suma = suma + 0.1
error2=100000000/10 - suma

print "\n************************************************************************"
print "Sumandolo 100 millones de veces, el error absoluto cometido es: %.6f" % error2
print "************************************************************************\n"

