.data
vector: .word 1, 4, 14, 5, 9, 6, 8, 12, 13, 15, 26, 8, 2, 0
size: .word 14

.text
.globl main

main:
la $a0, size            # $a0 = address of size
lw $t1, 0($a0)          # $t1 = size
addi $a0, $zero, 0      # Clear $a0

la $a2, vector          # $a2 = address of vector

loop:
lw $t2, 0($a2)          # Load first element of vector to $t2
addi $a2, $a2, 4        # Move address to next element
addi $t1, $t1, -1       # Decrease size

slt $t0, $a0, $t2       # Set $t0 to 1 if $a0 < $t2
bne $t0, $zero, mayor   # If element is less than max, jump to menor

loop_end:
bne $t1, $0, loop       # If size != 0, jump to lazo

addi $v0, $0, 10        # Set syscall to exit
syscall

mayor:
add $a0, $zero, $t2     # Set $a0 to $t2
j loop_end              # Return to finish loop
