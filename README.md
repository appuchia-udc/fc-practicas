# FC - Practicas

[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)

## Qué es esto

Aquí está el código que he escrito en las clases prácticas de Fundamentos de Computadores

## Licencia

El código está licenciado bajo la [licencia GPLv3](https://gitlab.com/appuchia/appuchia-udc/fc-practicas/-/blob/master/LICENSE).

Coded with 🖤 by Appu
